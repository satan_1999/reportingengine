#Trade Reporting Engine 

###Author : Ke (Wallace) Tan

Description: 
===
Create a Java program that reads a set of xml event files, extracts a set of elements (fields), filters
the events based on a set of criteria, and reports the events in comma separated value (CSV) format.

The eventN.xml files are included in the email and instructions. When reading the event xml files,
keep the Java code simple, consider using the following xml parser and Xpath reader included in the
JDK: javax.xml.parsers.DocumentBuilder, javax.xml.xpath.Xpath.

The following xml elements should be used for the filter criteria and then only these fields should be
printed as 'stdout' in CSV format, if the criteria below are met. The specified fields for each
event.xml should be converted to one CSV line with a newline character separating rows. E.g. –

EMU_BANK,LEFT_BANK,100.0,AUD

Xml elements (fields) to use
Format: xpath expression from event file => csv column header name

//buyerPartyReference/@href => buyer_party

//sellerPartyReference/@href => seller_party

//paymentAmount/amount => premium_amount

//paymentAmount/currency => premium_currency

The CSV formatted output must have column headers in the first row.

Filter Criteria
Only report events to CSV format if the following 3 criteria are true:

1. (The seller_party is EMU_BANK and the premium_currency is AUD) or (the seller_party is
BISON_BANK and the premium_currency is USD)
2. The seller_party and buyer_party must not be anagrams
Only events that match all criteria should be reported. 

Hypothesis 
====
* Only use the JDK and JUnit.


Design/Architecture 
====
* The application uses service orientated design. The main service class is _ReportingEngineService_.
* The parsed XML will be passed into a filter chain (_EventXMLRootFilter_) with _Chain Design Pattern_ to filter data with the criteria in each Filter.
* The structure is very scalable.
* The Design will follow Domain Driven Design and Test Driven Design.
* The application will load all xml files in the specific directory.

System Requirements 
====
* Java 8 is required.
* Maven 3.3.1+ is required.
* This project is a simple maven project.

How to run it 
====
To build the application and run all tests, please execute the following command in the root directory

      mvn clean install

To run the application with the default data file in _/src/main/resource_, please execute the following command in the root directory
      
      mvn exec:java -Dexec.mainClass="au.com.test.App"
   

This sample of output is below:

        buyer_party,seller_party,premium_amount,premium_currency
        LEFT_BANK,EMU_BANK,100.0,AUD
        LEFT_BANK,EMU_BANK,200.0,AUD
        EMU_BANK,BISON_BANK,500.0,USD
        EMU_BANK,BISON_BANK,600.0,USD


Auto-Test
====
There are 14 test cases.

To run all test cases, please execute the following command in the root directory
 
      mvn test

Further Improvement
====
* If we can use Spring Framework, we can use DI to bootstrap all beans and optimize the code structure.
* If we can use Mockito, we can add more Unit test case.