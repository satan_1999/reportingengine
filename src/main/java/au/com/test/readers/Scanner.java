package au.com.test.readers;

import java.io.File;
import java.util.List;

public interface Scanner {

    public List<File> searchFiles(String directory, String keyword);
}
