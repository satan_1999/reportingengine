package au.com.test.readers;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class XMLFileScanner implements Scanner {

    public List<File> searchFiles(String directory, String keyword) {
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        return Arrays.stream(listOfFiles).filter(file -> file.isFile() && file.getAbsolutePath().endsWith(".xml") && file.getName().contains(keyword)).collect(Collectors.toList());
    }
}
