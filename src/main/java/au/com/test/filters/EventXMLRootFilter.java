package au.com.test.filters;

import au.com.test.dto.CsvEntry;
import au.com.test.parsers.EventXMLParser;
import org.w3c.dom.Document;

import javax.xml.xpath.*;
import java.io.File;


public class EventXMLRootFilter extends AbstractDataFilter {

    private static final String XPATH_BUYER = "/requestConfirmation/trade/varianceOptionTransactionSupplement/buyerPartyReference/@href";
    private static final String XPATH_SELLER = "/requestConfirmation/trade/varianceOptionTransactionSupplement/sellerPartyReference/@href";
    private static final String XPATH_CURRENCY = "/requestConfirmation/trade/varianceOptionTransactionSupplement/equityPremium/paymentAmount/currency";
    private static final String XPATH_AMOUNT = "/requestConfirmation/trade/varianceOptionTransactionSupplement/equityPremium/paymentAmount/amount";
    private EventXMLParser eventXMLParser;
    private XPath xpath;

    public void init() {
        eventXMLParser = new EventXMLParser();
        xpath = XPathFactory.newInstance().newXPath();
        this.addNext(new SellerAndCurrencyFilter()).addNext(new CheckAnagramsFilter());
    }

    public CsvEntry.CsvEntryBuilder run(File xmlFile) {
        Document doc = eventXMLParser.parse(xmlFile);

        CsvEntry.CsvEntryBuilder csvEntryBuilder = new CsvEntry.CsvEntryBuilder().setFileName(xmlFile.getName());
        try {
            csvEntryBuilder.setBuyerParty((String) xpath.compile(XPATH_BUYER).evaluate(doc, XPathConstants.STRING))
                    .setSellerParty((String) xpath.compile(XPATH_SELLER).evaluate(doc, XPathConstants.STRING))
                    .setPremiumCurrency((String) xpath.compile(XPATH_CURRENCY).evaluate(doc, XPathConstants.STRING))
                    .setPremiumAmount((Double) xpath.compile(XPATH_AMOUNT).evaluate(doc, XPathConstants.NUMBER));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to parse XML File - " + xmlFile.getName());
        }
        return filter(csvEntryBuilder);
    }

    public CsvEntry.CsvEntryBuilder filter(CsvEntry.CsvEntryBuilder csvEntryBuilder) {
        return next().filter(csvEntryBuilder);
    }
}
