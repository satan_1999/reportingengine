package au.com.test.filters;

import au.com.test.dto.CsvEntry;
import au.com.test.exceptions.NotMatchException;

import java.util.Arrays;

public class CheckAnagramsFilter extends AbstractDataFilter {
    @Override
    public CsvEntry.CsvEntryBuilder filter(CsvEntry.CsvEntryBuilder csvEntryBuilder) {

        if (!isAnagram(csvEntryBuilder.getBuyerParty(), csvEntryBuilder.getSellerParty())) {
            return next() == null ? csvEntryBuilder : filter(csvEntryBuilder);
        }

        throw new NotMatchException("UnMatched File");
    }

    private boolean isAnagram(String firstWord, String secondWord) {
        char[] word1 = firstWord.trim().toCharArray();
        char[] word2 = secondWord.trim().toCharArray();
        Arrays.sort(word1);
        Arrays.sort(word2);
        return Arrays.equals(word1, word2);
    }
}
