package au.com.test.filters;


abstract class AbstractDataFilter implements DataFilter {

    private DataFilter nextHandler;

    public DataFilter next() {
        return nextHandler;
    }

    public DataFilter addNext(DataFilter nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }

}
