package au.com.test.filters;

import au.com.test.dto.CsvEntry;
import au.com.test.exceptions.NotMatchException;

public class SellerAndCurrencyFilter extends AbstractDataFilter {
    @Override
    public CsvEntry.CsvEntryBuilder filter(CsvEntry.CsvEntryBuilder csvEntryBuilder) {

        if ((csvEntryBuilder.getSellerParty().equalsIgnoreCase("EMU_BANK") && csvEntryBuilder.getPremiumCurrency().equalsIgnoreCase("AUD"))
                || (csvEntryBuilder.getSellerParty().equalsIgnoreCase("BISON_BANK") && csvEntryBuilder.getPremiumCurrency().equalsIgnoreCase("USD"))) {
            return next() == null ? csvEntryBuilder : next().filter(csvEntryBuilder);
        }

        throw new NotMatchException("UnMatched File - " + csvEntryBuilder.getFileName());
    }
}
