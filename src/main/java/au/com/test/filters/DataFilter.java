package au.com.test.filters;

import au.com.test.dto.CsvEntry;

public interface DataFilter {

    CsvEntry.CsvEntryBuilder filter(CsvEntry.CsvEntryBuilder builder);

    DataFilter addNext(DataFilter next);

}
