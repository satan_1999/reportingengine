package au.com.test.parsers;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;

public class EventXMLParser implements Parser {

    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    public Document parse(File xmlFile) {
        // create a new DocumentBuilderFactory

        Document doc = null;
        try {
            // use the factory to create a documentbuilder
            DocumentBuilder builder = factory.newDocumentBuilder();

            // create a new document from input source
            FileInputStream fis = new FileInputStream(xmlFile);
            InputSource is = new InputSource(fis);
            doc = builder.parse(is);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return doc;
    }
}
