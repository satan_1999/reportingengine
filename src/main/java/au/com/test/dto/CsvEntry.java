package au.com.test.dto;


public class CsvEntry {
    private String buyerParty;
    private String sellerParty;
    private Double premiumAmount;
    private String premiumCurrency;

    public static String header() {
        return "buyer_party" + "," +
                "seller_party" + "," + "premium_amount" + "," + "premium_currency";
    }

    public String toCSVFormat() {
        return this.buyerParty + "," +
                sellerParty + "," + premiumAmount + "," + premiumCurrency;
    }

    public String toString() {
        return toCSVFormat();
    }


    public static class CsvEntryBuilder {
        private String buyerParty;
        private String sellerParty;
        private Double premiumAmount;
        private String premiumCurrency;
        private String fileName;

        public CsvEntryBuilder setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public String getFileName() {
            return fileName;
        }

        public CsvEntryBuilder setBuyerParty(String buyerParty) {
            this.buyerParty = buyerParty;
            return this;
        }

        public CsvEntryBuilder setSellerParty(String sellerParty) {
            this.sellerParty = sellerParty;
            return this;
        }

        public CsvEntryBuilder setPremiumAmount(Double premiumAmount) {
            this.premiumAmount = premiumAmount;
            return this;
        }

        public CsvEntryBuilder setPremiumCurrency(String premiumCurrency) {
            this.premiumCurrency = premiumCurrency;
            return this;
        }

        public String getBuyerParty() {
            return buyerParty;
        }

        public String getSellerParty() {
            return sellerParty;
        }

        public Double getPremiumAmount() {
            return premiumAmount;
        }

        public String getPremiumCurrency() {
            return premiumCurrency;
        }

        public CsvEntry build() {
            CsvEntry entry = new CsvEntry();
            entry.buyerParty = this.buyerParty;
            entry.sellerParty = this.sellerParty;
            entry.premiumAmount = this.premiumAmount;
            entry.premiumCurrency = this.premiumCurrency;
            return entry;
        }
    }

}
