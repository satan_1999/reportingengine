package au.com.test.service;

import au.com.test.dto.CsvEntry;
import au.com.test.exceptions.NotMatchException;
import au.com.test.filters.EventXMLRootFilter;
import au.com.test.readers.Scanner;
import au.com.test.readers.XMLFileScanner;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReportingEngineService {

    public static final String EVENT = "event";
    private EventXMLRootFilter eventXMLFilterChain;
    private Scanner xmlFileScanner;

    /**
     * Initialize the service
     */
    public void init() {
        eventXMLFilterChain = new EventXMLRootFilter();
        eventXMLFilterChain.init();
        xmlFileScanner = new XMLFileScanner();

    }

    public List<CsvEntry> reportEMUAndBISONEvents(String directory) {
        List<File> xmlFiles = xmlFileScanner.searchFiles(directory, EVENT);
        return xmlFiles.stream().map(this::filterEventXML).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private CsvEntry filterEventXML(File xmlFile) {
        try {
            return eventXMLFilterChain.run(xmlFile).build();
        } catch (Exception e) {
            if (!(e instanceof NotMatchException)) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
