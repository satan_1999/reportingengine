package au.com.test;

import au.com.test.dto.CsvEntry;
import au.com.test.service.ReportingEngineService;

import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        try {
            ReportingEngineService service = new ReportingEngineService();
            service.init();
            List<CsvEntry> entries = service.reportEMUAndBISONEvents(App.class.getClassLoader().getResource(".").getPath());

            System.out.println(CsvEntry.header());
            entries.forEach(entry -> System.out.println(entry.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
