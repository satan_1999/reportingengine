package au.com.test.parsers;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;

public class EventXMLParserTest {
    private EventXMLParser parser = new EventXMLParser();

    @Test
    public void testParse_ShouldReturnDocument() throws Exception {
        File file = new File(this.getClass().getClassLoader().getResource("./good/event0.xml").getPath());
        Document result = parser.parse(file);
        Assert.assertNotNull(result);
    }

}