package au.com.test.filters;

import au.com.test.dto.CsvEntry;
import au.com.test.exceptions.NotMatchException;
import org.junit.Assert;
import org.junit.Test;

public class CheckAnagramsFilterTest {
    private CheckAnagramsFilter filer = new CheckAnagramsFilter();

    @Test(expected = NotMatchException.class)
    public void testFilter_ThrowNotMatchException() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("AVE_").setSellerParty("EAV_");
        filer.filter(builder);
    }

    @Test
    public void testFilter_ShouldBeOk() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("EMU_BANK");
        Assert.assertNotNull(filer.filter(builder));
    }

}