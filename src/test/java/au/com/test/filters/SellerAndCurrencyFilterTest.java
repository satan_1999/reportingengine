package au.com.test.filters;

import au.com.test.dto.CsvEntry;
import au.com.test.exceptions.NotMatchException;
import org.junit.Assert;
import org.junit.Test;

public class SellerAndCurrencyFilterTest {

    private SellerAndCurrencyFilter filer = new SellerAndCurrencyFilter();

    @Test(expected = NotMatchException.class)
    public void testFilter() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("V").setPremiumCurrency("AUD");
        filer.filter(builder);
    }

    @Test(expected = NotMatchException.class)
    public void testFilter_WhenSellerIsEMU_BANKAndCurrencyIsUSD() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("EMU_BANK").setPremiumCurrency("USD");
        filer.filter(builder);
    }

    @Test(expected = NotMatchException.class)
    public void testFilter_WhenSellerIsBISON_BANKAndCurrencyIsAUD() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("BISON_BANK").setPremiumCurrency("AUD");
        filer.filter(builder);
    }

    @Test
    public void testFilter_ShouldBeOk_WhenSellerIsEMU_BANKAndCurrencyIsUSD() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("EMU_BANK").setPremiumCurrency("AUD");
        Assert.assertNotNull(filer.filter(builder));
    }

    @Test
    public void testFilter_ShouldBeOk_WhenSellerIsBISON_BANKAndCurrencyIsAUD() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("BISON_BANK").setPremiumCurrency("USD");
        Assert.assertNotNull(filer.filter(builder));
    }

}