package au.com.test.dto;

import org.junit.Assert;
import org.junit.Test;

public class CsvEntryTest {

    @Test
    public void testToCSVFormat() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("V").setPremiumCurrency("AUD").setPremiumAmount(1.1);
        Assert.assertEquals("A,V,1.1,AUD", builder.build().toCSVFormat());
    }

    @Test
    public void testToString() throws Exception {
        CsvEntry.CsvEntryBuilder builder = new CsvEntry.CsvEntryBuilder().setBuyerParty("A").setSellerParty("V").setPremiumCurrency("AUD").setPremiumAmount(1.1);
        Assert.assertEquals("A,V,1.1,AUD", builder.build().toString());
    }

}