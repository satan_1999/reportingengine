package au.com.test.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ReportingEngineServiceTest {
    private ReportingEngineService service;

    @Before
    public void setUp() {
        service = new ReportingEngineService();
        service.init();
    }

    @Test
    public void reportEMUAndBISONEvents_ShouldReturn6Entries() throws Exception {
        List result = service.reportEMUAndBISONEvents(this.getClass().getClassLoader().getResource("./good/").getPath());
        Assert.assertEquals(4, result.size());
    }

    @Test
    public void reportEMUAndBISONEvents_ShouldReturn0Entries_() throws Exception {
        List result = service.reportEMUAndBISONEvents(this.getClass().getClassLoader().getResource("./bad/").getPath());
        Assert.assertEquals(0, result.size());
    }

}