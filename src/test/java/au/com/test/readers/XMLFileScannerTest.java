package au.com.test.readers;

import au.com.test.App;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class XMLFileScannerTest {

    @Test
    public void testSearchFiles_ShouldReturn4Files() throws Exception {
        XMLFileScanner scanner = new XMLFileScanner();
        List<File> files = scanner.searchFiles(App.class.getClassLoader().getResource(".").getPath()+"good/", "event");

        Assert.assertEquals(4, files.size());
    }

    @Test
    public void testSearchFiles_ShouldReturn0Files() throws Exception {
        XMLFileScanner scanner = new XMLFileScanner();
        List<File> files = scanner.searchFiles(App.class.getClassLoader().getResource(".").getPath()+"/badFiles", "event");

        Assert.assertEquals(0, files.size());
    }

}